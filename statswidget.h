#ifndef STATSWIDGET_H
#define STATSWIDGET_H

#include <QWidget>
#include "svninfo.h"

namespace Ui {
class statswidget;
}

class StatsWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit StatsWidget(QWidget *parent = 0);
    void setInfo(SvnInfo *info);
    ~StatsWidget();

public slots:
    void showTab(int tabIndex);
    
private:
    Ui::statswidget *ui;
    void getTableData(QString &folderPath);
};

#endif // STATSWIDGET_H
