#ifndef OPENREPOSITORYWIDGET_H
#define OPENREPOSITORYWIDGET_H

#include <QWidget>
#include <vector>
#include <algorithm>
#include "mainwindow.h"

namespace Ui {
class OpenRepositoryWidget;
}

class OpenRepositoryWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit OpenRepositoryWidget(QWidget *parent = 0);
    ~OpenRepositoryWidget();
    
private slots:
    void on_locationButton_clicked();
    void on_openNewButton_clicked();
    void on_openRecentButton_clicked();

private:
    Ui::OpenRepositoryWidget *ui;
    std::vector<SvnInfo*> repoList;
    void loadRepoFromFile();
    void saveRepoToFile();
    void addRepoToList(SvnInfo *info)
    {
        if(std::find(repoList.begin(),repoList.end(), info) == repoList.end())
            repoList.push_back(info);
    }

};

#endif // OPENREPOSITORYWIDGET_H
