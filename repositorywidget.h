#ifndef REPOSITORYWIDGET_H
#define REPOSITORYWIDGET_H

#include <QWidget>
#include <QMap>
#include "statswidget.h"
#include "svninfo.h"

namespace Ui {
class RepositoryWidget;
}

class RepositoryWidget : public QWidget
{
    Q_OBJECT
    SvnInfo *info;
    QString revision;
    
public:
    explicit RepositoryWidget(QWidget *parent = 0);
    bool loadRepository(SvnInfo *info);
    StatsWidget *statsWidget();
    ~RepositoryWidget();

private slots:
    void on_revisions_itemSelectionChanged();

    void on_files_doubleClicked(const QModelIndex &index);

private:
    Ui::RepositoryWidget *ui;
    StatsWidget *stats;
};

#endif // REPOSITORYWIDGET_H
