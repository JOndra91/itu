#include "filediffwidget.h"
#include "ui_filediffwidget.h"

FileDiffWidget::FileDiffWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FileDiffWidget)
{
    ui->setupUi(this);
}

FileDiffWidget::~FileDiffWidget()
{
    delete ui;
}

void FileDiffWidget::appendDiff(QString &line)
{
    QColor c;

    if(line.endsWith("\n"))
    {
        line.remove("\n");
    }

    if(line.startsWith('+'))
    {
        c.setNamedColor("blue");
    }
    else if(line.startsWith('-'))
    {
        c.setNamedColor("red");
    }
    else if(line.startsWith('@'))
    {
        c.setNamedColor("green");
    }
    else
    {
        c.setNamedColor("black");
    }

    ui->textBrowser->setTextColor(c);
    ui->textBrowser->append(line);
}
