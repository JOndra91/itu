#include "openrepositorywidget.h"
#include "ui_openrepositorywidget.h"
#include <QFileDialog>

OpenRepositoryWidget::OpenRepositoryWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OpenRepositoryWidget)
{
    ui->setupUi(this);

    loadRepoFromFile();
}

void OpenRepositoryWidget::on_locationButton_clicked()
{
    QFileDialog *fDialog = new QFileDialog(this,"Vyberte adresář",ui->locationEdit->text(),NULL);
    fDialog->setFileMode(QFileDialog::DirectoryOnly);
    fDialog->setOption(QFileDialog::ShowDirsOnly);

    if(fDialog->exec())
    {
        ui->locationEdit->setText(fDialog->selectedFiles()[0]);
    }
}

void OpenRepositoryWidget::on_openNewButton_clicked()
{
    MainWindow *parent = (MainWindow*)this->parent();

    QList<QLineEdit*> fields;
    fields.append(ui->loginEdit);
    fields.append(ui->passwordEdit);
    fields.append(ui->urlEdit);
    fields.append(ui->locationEdit);

    QList<QLineEdit*>::iterator i;
    for(i = fields.begin(); i != fields.end(); ++i)
    {
        if((*i)->text().isEmpty())
        {
            QMessageBox msg;
            msg.setText("Vyplňte všechna pole");
            msg.exec();
            (*i)->setFocus();
            return;
        }
    }

    SvnInfo *info = new SvnInfo;
    info->login = ui->loginEdit->text();
    info->passwd = ui->passwordEdit->text();
    info->repoUrl = ui->urlEdit->text();
    info->folderPath = ui->locationEdit->text();

    addRepoToList(info);

    parent->setInfo(info);

    parent->showRepository();
}

void OpenRepositoryWidget::on_openRecentButton_clicked()
{
    MainWindow *parent = (MainWindow*)this->parent();

    int row = ui->recentList->currentRow();

    if(row == -1)
    {
        QMessageBox msg(QMessageBox::NoIcon, "", "Pro pokračování zvolte prosím repozitář.");
        msg.exec();
        return;
    }

    parent->setInfo(repoList.at(row));

    parent->showRepository();
}


OpenRepositoryWidget::~OpenRepositoryWidget()
{
    saveRepoToFile();
    delete ui;
}

void OpenRepositoryWidget::loadRepoFromFile()
{
    QFile file(QDir::currentPath() + QDir::separator() + "svnrepos.cvs");

    if(!file.open(QFile::ReadOnly))
        return;

    QString line;
    QStringList list;
    SvnInfo *info;

    while(!file.atEnd())
    {
        info = new SvnInfo;

        line = file.readLine().trimmed();

        list = line.split(",");

        if(list.size() != 4)
            continue;

        info->login = list.at(0);
        info->passwd = list.at(1);
        info->repoUrl = list.at(2);
        info->folderPath = list.at(3);

        repoList.push_back(info);
        ui->recentList->addItem(info->toListString());
    }

    file.close();
}

void OpenRepositoryWidget::saveRepoToFile()
{
    QFile file(QDir::currentPath() + QDir::separator() + "svnrepos.cvs");

    if(!file.open(QFile::WriteOnly))
        return;

    foreach (SvnInfo *repo, repoList) {
        file.write(repo->toQString().toLocal8Bit());
    }
    file.close();
}
