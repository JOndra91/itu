#include "statswidget.h"
#include "ui_statswidget.h"
#include <QDir>

StatsWidget::StatsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::statswidget)
{
    ui->setupUi(this);
}

StatsWidget::~StatsWidget()
{
    delete ui;
}

void StatsWidget::setInfo(SvnInfo *info)
{
    QImage timeChart(info->folderPath + QDir::separator() + "time_chart.png");
    QImage userChart(info->folderPath + QDir::separator() + "user_chart.png");

    ui->timeChartLabel->setPixmap(QPixmap::fromImage(timeChart));
    ui->userChartLabel->setPixmap(QPixmap::fromImage(userChart));
    getTableData(info->folderPath);
}

void StatsWidget::showTab(int tabIndex)
{
    ui->tabWidget->setCurrentIndex(tabIndex);

    if(isHidden())
    {
        setVisible(true);
    }

    activateWindow();
}

void StatsWidget::getTableData(QString &folderPath)
{
    QFile file(folderPath + QDir::separator() + "commit_table.cvs");

    if(!file.open(QFile::ReadOnly))
        return;

    int i = 0;
    QString line;
    QStringList list;
    QTableWidget *table = ui->tableWidget;

    table->verticalHeader()->setVisible(false);
    while(!file.atEnd())
    {
        line = file.readLine().trimmed();

        list = line.split(",");

        if(list.size() != 5)
            continue;
        table->insertRow(i);
        table->setItem(i,0, new QTableWidgetItem(list.at(0)));
        table->setItem(i,1, new QTableWidgetItem(list.at(1)));
        table->setItem(i,2, new QTableWidgetItem(list.at(2)));
        table->setItem(i,3, new QTableWidgetItem(list.at(3)));
        table->setItem(i,4, new QTableWidgetItem(list.at(4)));

        ++i;
    }

    file.close();
}
