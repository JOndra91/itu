#ifndef FILEDIFFWIDGET_H
#define FILEDIFFWIDGET_H

#include <QWidget>

namespace Ui {
class FileDiffWidget;
}

class FileDiffWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit FileDiffWidget(QWidget *parent = 0);
    void appendDiff(QString &line);
    ~FileDiffWidget();
    
private:
    Ui::FileDiffWidget *ui;
};

#endif // FILEDIFFWIDGET_H
