#-------------------------------------------------
#
# Project created by QtCreator 2013-11-24T10:27:02
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = itu
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    openrepositorywidget.cpp \
    repositorywidget.cpp \
    filediffwidget.cpp \
    statswidget.cpp

HEADERS  += mainwindow.h \
    openrepositorywidget.h \
    repositorywidget.h \
    filediffwidget.h \
    svninfo.h \
    statswidget.h

FORMS    += mainwindow.ui \
    openrepositorywidget.ui \
    repositorywidget.ui \
    filediffwidget.ui \
    statswidget.ui
