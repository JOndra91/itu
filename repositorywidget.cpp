#include "repositorywidget.h"
#include "ui_repositorywidget.h"
#include <QDir>
#include <QFile>
#include <filediffwidget.h>

RepositoryWidget::RepositoryWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RepositoryWidget)
{
    revision.clear();
    stats = new StatsWidget();
    ui->setupUi(this);
}

bool RepositoryWidget::loadRepository(SvnInfo *info)
{
    QFile f(info->folderPath + QDir::separator() + "revisions.list");
    QString line;
    SvnRevision *rev = NULL;
    QTreeWidgetItem *item;
    int i = 0;

    this->info = info;

    if(f.open(QFile::ReadOnly))
    {
        stats->setInfo(info);

        while(!f.atEnd())
        {
            line = f.readLine().trimmed();
            if(line == QString("commit"))
            {
                if(rev != NULL)
                {
                    item = new QTreeWidgetItem(rev->revision);
                    item->setText(0,QString("%1").arg(rev->revision,3,10,QChar('0')));
                    item->setText(1, rev->message);
                    item->setText(2,rev->author);
                    item->setText(3, rev->date);

                    ui->revisions->addTopLevelItem(item);

                    i++;
                }

                rev = new SvnRevision;
                rev->revision = i+1;

                rev->author = f.readLine();
                rev->date = f.readLine();

                line = f.readLine();
            }

            if(rev != NULL)
            {
                rev->message += line;
            }
        }

        if(rev != NULL)
        {
            item = new QTreeWidgetItem(rev->revision);
            item->setText(0,QString("%1").arg(rev->revision,3,10,QChar('0')));
            item->setText(1, rev->message);
            item->setText(2,rev->author);
            item->setText(3, rev->date);

            ui->revisions->addTopLevelItem(item);

            i++;
        }

        f.close();
    }
    else
    {
        return false;
    }

    return true;
}

RepositoryWidget::~RepositoryWidget()
{
    delete ui;
    delete stats;
}

void RepositoryWidget::on_revisions_itemSelectionChanged()
{
    QString line, filename;
    QListWidgetItem *item;
    revision = ui->revisions->selectedItems().first()->text(0);

    QFile f(info->folderPath + QDir::separator() + revision + ".rev");

    ui->files->clear();

    if(f.open(QFile::ReadOnly))
    {
        while(!f.atEnd())
        {
            line = f.readLine();

            if(line.startsWith(QChar('$')))
            {
                filename = line.remove(0,2).trimmed();

                item = new QListWidgetItem(filename);
                ui->files->addItem(item);
            }
        }

        f.close();
    }
}


void RepositoryWidget::on_files_doubleClicked(const QModelIndex &index)
{
    QString filename, line;
    QListWidgetItem *item;
    item = ui->files->item(index.row());

    filename = item->text();

    QFile f(info->folderPath + QDir::separator() + revision + ".rev");

    if(f.open(QFile::ReadOnly))
    {
        FileDiffWidget *diffWidget = new FileDiffWidget();

        diffWidget->setWindowTitle(filename);

        while(!f.atEnd())
        {
            line = f.readLine();

            if(line.startsWith(QChar('$')))
            {
                if(filename == line.remove(0,2).trimmed())
                {
                    break;
                }
            }
        }

        while(!f.atEnd())
        {
            line = f.readLine();

            if(line.startsWith(QChar('$')))
            {
                break;
            }
            else
            {
                diffWidget->appendDiff(line);
            }
        }

        f.close();
        diffWidget->setVisible(true);
    }
}

StatsWidget *RepositoryWidget::statsWidget()
{
    return stats;
}
