#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "openrepositorywidget.h"
#include "repositorywidget.h"
#include "filediffwidget.h"
#include <QMessageBox>
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    showMenuBar(false);

    mapper = new QSignalMapper(this);
    QWidget *widget = new OpenRepositoryWidget(this);
    delete swapCentralWidget(widget);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QWidget* MainWindow::swapCentralWidget(QWidget *widget)
{
    QWidget *oldWidget = centralWidget();
    setCentralWidget(widget);
    return oldWidget;
}

void MainWindow::showMenuBar(bool show)
{
    menuBar()->clear();

    if(show)
    {
        menuBar()->addAction(ui->menuFile->menuAction());
        menuBar()->addAction(ui->menuStats->menuAction());
    }
}

void MainWindow::showRepository()
{
    RepositoryWidget *widget = new RepositoryWidget(this);
    if(!widget->loadRepository(this->info))
    {
        QMessageBox msg(QMessageBox::NoIcon, "Chyba", "Nelze se připojit k repozitáři!");
        msg.exec();
        return;
    }

    connect(ui->actionTimeChart, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(ui->actionTimeChart, 0);
    connect(ui->actionUserChart, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(ui->actionUserChart, 1);
    connect(ui->actionUserTable, SIGNAL(triggered()), mapper, SLOT(map()));
    mapper->setMapping(ui->actionUserTable, 2);
    connect(mapper,SIGNAL(mapped(int)), widget->statsWidget(), SLOT(showTab(int)));

    delete swapCentralWidget(widget);
    setWindowTitle("SVN klient");
    showMenuBar(true);
    resize(widget->size().width(),widget->size().height());
}

void MainWindow::on_actionOpen_triggered()
{
    QWidget *widget = new MainWindow();
    widget->setVisible(true);
}

void MainWindow::on_actionQuit_triggered()
{
    close();
}
