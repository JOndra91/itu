#ifndef SVNINFO_H
#define SVNINFO_H

#include <QString>

struct SvnInfo
{
    QString login;
    QString passwd;
    QString repoUrl;
    QString folderPath;

    QString toQString()
    {
        return (login + "," + passwd + "," + repoUrl + "," + folderPath + "\n");
    }

    QString toListString()
    {
        return (login + " - " + folderPath);
    }

    bool operator==(const SvnInfo info)
    {
        return (login == info.login &&
                passwd == info.passwd &&
                repoUrl == info.repoUrl &&
                folderPath == info.folderPath);
    }
};

struct SvnRevision
{
    int revision;
    QString author;
    QString date;
    QString message;
};

#endif // SVNINFO_H
