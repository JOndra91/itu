#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets>
#include "svninfo.h"

namespace Ui {
class MainWindow;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    void showMenuBar(bool show);
    void showRepository();

    ~MainWindow();

    void setInfo(SvnInfo* _info) { info = _info; }
    SvnInfo* getInfo() { return info; }

private slots:
    void on_actionOpen_triggered();
    void on_actionQuit_triggered();

private:
    Ui::MainWindow *ui;
    SvnInfo *info;
    QWidget* swapCentralWidget(QWidget *widget);
    QSignalMapper *mapper;
};

#endif // MAINWINDOW_H
